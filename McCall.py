# -*- coding: utf-8 -*-
"""
Created on Thu Feb 18 16:13:52 2016

@author: David Evans
"""

import numpy as np

def bellmanTmap(beta,w,pi,b,alpha,chi,Vcont,Qcont):
    '''
    Parameters
    ----------
    beta - discount factor beta (float)
    w - vector of possible wages (length S numpy array)
    pi - vector of probabilities (length S array)
    b - unemployment insurance (float)
    alpha - firing probability (float)
    chi - effort cost of searching (float)
    Vcont - continuations values with wage offer  w(s) (length S array)
    Qcont - continuation value of unemployment (float)
    
    Returns
    -------
    V - current value of having a wage offer w(s) (length S numpy array)
    Q - current value of being unemployed (float)
    C - C[s]=1 if worker accepts wage offer w(s), 0 otherwise (length S array)
    E - true if the worker will put effort to search (boolean)
    
    
    Given equations
    ---------------
    E[SUM((beta**t)*c(t)] <- we want to maximize this
    
    Q(t) = max{b + beta*(0.3*SUM(pi(s')*V(t+1)[s'])+.7Q(t+1)), b - chi + beta*(0.5*SUM(pi(s')*V(t+1)[s'])+ .5Q(t+1))}
    V(t)[s] = max{ w(s)+beta*(alpha*Q(t+1)+(1-alpha)SUM(pi(s')*V(t+1)[s'])), Q(t) }
    '''
    
    pi_dot_V = pi.dot(Vcont)
    
    #defining Bellman Q Equation
    Q_no_effort = b + beta*(0.3*pi_dot_V + .7*Qcont)
    Q_effort = b - chi + beta*(.5*pi_dot_V + .5*Qcont)
    #maximizing over the two options
    Q = np.maximum(Q_no_effort, Q_effort)
    
    #defining Bellman V equation
    V_accept = w + beta*(alpha*Qcont + (1-alpha)*Vcont)
    # Q represents not accepting the wage offers
    # maximize over the two options: V_accept and Q
    V = np.maximum(V_accept, Q)
    
    C_bool = (V==V_accept)
    C= 1*C_bool
    
    E = bool(Q_effort > Q_no_effort)

    
    return V, Q, C, E
    pass
    
    
    
    
def solveInfiniteHorizonProblem(beta,w,pi,b,alpha,chi,epsilon=1e-8):
    '''
    Parameters
    ----------
    beta - discount factor beta (float)
    w - vector of possible wages (length S numpy array)
    pi - vector of probabilities (length S array)
    b - unemployment insurance (float)
    alpha - firing probability (float)
    chi - effort cost of searching (float)
    epsilon - convergence criteria in norm of V-T(V), default = 1e-8 (float)
    
    Returns
    -------
    V - current value of having a wage offer w(s) (length S numpy array)
    Q - current value of being unemployed (float)
    C - C[s]=1 if worker accepts wage offer w(s), 0 otherwise (length S array)
    E - true if the worker will put effort to search (boolean)
    '''
    V=np.zeros(len(w))
    Q= 0.
    diff = 1
    while diff >epsilon:
        V_new, Q_new, C, E = bellmanTmap(beta, w, pi, b, alpha, chi, V, Q)
        diff = np.linalg.norm(V-V_new)
        V=V_new
        Q=Q_new
    
    return V, Q, C, E
        
    
    pass
    
def HazardRate(pi,C,E):
    '''
    Computes the hazard rate of leaving unemployment in Problem 1

    Parameters
    ----------
    pi - vector of probabilities of having a wage offer (length S numpy array)
    C - C[s] = 1 if worker accepts wage offer W(s), 0 otherwise (length S array)
    E - true if the worker will put effort into the job search (boolean)
    
    Returns
    --------
    hazard_rate - hazard rate of leaving unemployment 
    
    (In words the hazard rate is the probability that the worker will recieve
     a wage offer times the probability that they will accept it)
    '''
    
    E_integer = 1*E
    pi_dot_c = pi.dot(C)
    if E_integer == 1:
        E_probability = .5
    else:
        E_probability = .3
    H = pi_dot_c * E_probability
    
    return H
          
    pass
    

        
        
def bellmanTmap_HC(beta,w,pi,b,alpha,p_h,h,Vcont,Qcont):
    '''
    Iterates of Bellman Equation of Problem 2
    
    Parameters
    ----------
    beta - discount factor beta
    w - vector of possible wages
    pi - vector of probabilities for each wage
    b - unemployment insurance
    alpha - firing probability
    p_h - probability of transitioning human capital states
    h - vector of human capital levels
    Vcont - continuation values for wage offer and human capital
    Qcont - continuation values of unemployment with human capital 
    
    Returns
    -------
    V - current value of having a specific wage offer and human capital
    Q - current value of being unemployed with specified human capital
    C - indicator for whether or not a worker accepts the wage offer
    '''
    
    J=len(h)
    S=len(w)
    V=np.zeros((J, S))
    Q=np.zeros(J)
    C=np.zeros((J, S))
    
    for j in range(0, J):
        if j == 0:
            Q[0] = b + beta *pi.dot(Vcont[0,:])
        else:
            Q[j] = b + beta*pi.dot(p_h*Vcont[j-1,:]+(1-p_h)*Vcont[j,:])
        if j == J-1:
            V_accept = w*h[j] + beta*(alpha*Qcont[j] + (1-alpha)*Vcont[j,:])
        else:
            Qhat=p_h*Qcont[j+1] + (1-p_h)*Qcont[j]
            Vhat=p_h*Vcont[j+1,:] + (1-p_h)*Vcont[j,:]
            V_accept = w*h[j] + beta*(alpha*Qhat + (1-alpha)*Vhat)
        V[j] = np.maximum(V_accept, Q[j])
        C[j] = (V_accept==V[j])
        
    
    return V, Q, C

    pass

def solveInfiniteHorizonProblem_HC(beta,w,pi,b,alpha,p_h,h,epsilon=1e-8):
    '''
    Solve infinite horizon workers problem of Problem 2
    See Homework 3.pdf for Documentation
    '''

    J=len(h)
    S=len(w)
    V=np.zeros((J, S))
    Q=np.zeros(J)
    C=np.zeros((J, S))
    
    diff = 1.
    while diff > epsilon:
        V_new, Q_new, C= bellmanTmap_HC(beta,w,pi,b,alpha,p_h,h,V,Q)
        diff = np.linalg.norm(V-V_new)
        V=V_new
        Q=Q_new
    
    return V,Q,C
    
    pass
    
    
    
    