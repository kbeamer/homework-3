# -*- coding: utf-8 -*-
import numpy as np
import McCall as MC
import scipy.stats as stats
import matplotlib.pyplot as plt

'''
Parameters
'''
beta = 0.995
alpha = 0.0385
S = 100
chi = 10.
b = 10.

#take distribution of wages to approximate logw~N(log(20.),1)
logw_dist = stats.norm(np.log(20.),.5)
logw = logw_dist.ppf(np.linspace(1./(2*S),1-1./(2*S),S))
w = np.exp(logw)
pi = np.ones(S)/S


'''
Problem 1.
'''

#part a
V,Q = {}, {}
V[10]=np.zeros(S)
Q[10]=0.
for t in reversed(range(10)):
    V[t],Q[t],_,_ =MC.bellmanTmap(beta,w,pi,b,alpha,chi, V[t+1], Q[t+1])
print(Q[0])
# right now I am getting output of 187.532595042
# the pdf suggests I should get 187.53259504 

# DONE with this problem

#part b

V,Q,C,E = MC.solveInfiniteHorizonProblem(beta,w,pi,b,alpha,chi)
print(Q)
print(E)
# right now I am getting output of V = 5768.56123231 and E=true
# I am supposed to get V=5768.56123231 and E=True

# DONE with this problem

#part c

def hazard(b):
    V, Q, C, E = MC.solveInfiniteHorizonProblem(beta,w,pi,b,alpha,chi)
    return MC.HazardRate(pi, C, E)

b_grid = np.linspace(10., 20., 20.)
plt.figure(figsize=plt.figaspect(0.5))
plt.plot(b_grid, [hazard(b0) for b0 in b_grid])
# I am getting the correct graph to match the figure pictured in the assignment

# DONE with this problem


'''
Problem 2
'''

# part a

J = 20
h = np.linspace(.5,1.5,J)
p_h = 0.05
V,Q = {},{}
V[10] = np.zeros((J,S))
Q[10] = np.zeros(J)
for t in reversed(range(10)):
    V[t],Q[t],_ = MC.bellmanTmap_HC(beta,w,pi,b,alpha,p_h,h,V[t+1],Q[t+1])
print(Q[0])

# I am getting the correct vector to match the values stated in the assignment

# DONE with this problem

# part b

V,Q,C = MC.solveInfiniteHorizonProblem_HC(beta,w,pi,b,alpha,p_h,h)
hazard_rate = []
for j in range(J):
    hazard_rate.append(pi.dot(C[j]))
plt.figure(figsize=plt.figaspect(0.5))
plt.xlabel('h')
plt.ylabel('Hazard Rate')
plt.plot(h,hazard_rate)


# I am getting the correct graph - it matches the figure in the homework

# DONE with this problem

